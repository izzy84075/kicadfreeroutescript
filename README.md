I missed having the Freerouting button in the toolbar in Kicad 5.x, so I dove into the scripting a little bit to bring back at least the launcher button.

You'll still have to export the .dsn and import the .ses file manually, but by far the slowest part for me was getting Freerouting to actually open the .dsn file because of it's obnoxious defaulting to the Documents folder, so this script launches FreeRouting.jar and passes it the default filename that KiCAD will pick for the .dsn, saving you possibly dozens of clicks!

Setup:

- Place the [.py file](https://bitbucket.org/izzy84075/kicadfreeroutescript/src/master/freeroute.py) in %APPDATA%\kicad\scripting\plugins (On Windows) or ~\\.kicad\scripting\plugins (On Linux, not sure about OS X).
- Place FreeRouting.jar (I use [this version](https://github.com/freerouting/freerouting)) in %APPDATA%\kicad\ (On Windows) or ~\\.kicad\ (On Linux, not sure about OS X).
- In Pcbnew, under Tools -> External Plugins there should now be a Freeroute entry (If not, click the "Refresh Plugins" entry and try again).
- On nightlies(Or 5.1 when it releases), you can also go into the Preferences and under Action Plugins, you can set the plugin to show directly on the toolbar.

Now, the workflow for using this:

1. Export DSN (File -> Export -> Specctra DSN...) using the default filename for your PCB (board.kicad_pcb would be board.dsn, for example).
2. Click the Freeroute button/tool entry. This should launch the usual Freeroute GUI, with your design already loaded. NOTE: The KiCAD UI might be unresponsive at this point, as it's waiting for Freeroute to finish running.
3. Run your autoroute in Freeroute (Click the Autorouter button, wait, File -> Export Specctra Session File), then exit Freeroute.
4. Back in KiCAD, Import the session file (File -> Import -> Specctra Session...).
5. Done!